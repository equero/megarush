# Megarush Technical Challenge

First of all, thank you very much for the opportunity and I hope you enjoy the test.

Start the project in ur local environment `npm run serve`

[Live DEMO](http://165.227.224.151/)

## Framework and libraries
### Vue + Vuex
Since the test is made for the hiring of a vue developer, I decided to use this framework, although I had not used it for a long time. I use vuex to use the store manager for the data

### vue-class-component
I have used the [vue-class-component](https://www.npmjs.com/package/vue-class-component) library, because I have decided to use a class-style typescript in order to show how it looks in big projects. 

### vue-toasted
At first I was willing to create my own customised one, but since I didn't want to spend more time on the test, I borrowed one that was already functional.
[Library webpage](https://www.npmjs.com/package/vue-toasted)

## Technical Debts
- [X] Store fictional (or real) game record
- [X] Assure at least two routable views
- [X] Implement CRUD
- [X] Allow direct linking to particular game details page
- [X] Make sure pages response to window size change with at least two breakpoints
- [X] Make sure the app maintains game records even if the browser window is reloaded (it can forget them when browser gets closed)
- [X] Deploy it to your favourite online hosting service
- [X] Mimic architecture of full scale web application
- [X] Code preferably with TypeScript
- [X] Make the UI responsive - layout needs to look good on
- [ ] Unit tests

## Technical Explanation
### Why haven't I done unit tests?
At first it seemed a bit absurd to do unit tests for such a simple page in terms of functionality, it would have made more sense to do functional tests. So I decided to leave it for the end. When I finished all the functionality, I realized that I had already spent a lot of time and that Mocha + Chai is not a framework I'm used to, so I decided to retire it to give it some design.

### Is that the final design?
Yes and no. I'm VERY BAD at designing web pages, although I think I'm pretty good at web layout designer. Maybe I need more practice with grid layout, but nothing I can't learn quickly. The problem is like everything else, time. I didn't want to spend more than 14 hours of work and for that I had to sacrifice some design.

### How many hours did it take you to do the challenge?
14 hours to be exact. I would have loved to continue it and make it perfect, but it seems to me that going over 14 hours wouldn't be fair and two afternoons I don't think would be more than 12 hours.

### Why haven't you split HTML, scss and TS?
One thing I love about Vue is having everything in the same file. I could separate it, as I know it's a very common way to use it in big projects, but still, for lack of time and convenience for myself I've decided to do it this way.

### How much experience do you have with vue?
I love vue, and it is my language of choice for personal projects, but my professional experience is limited to a few years(2 or 3). Unlike Angular, which I've been using since Angularjs started. So moving from Angularjs to Vue3 on the fly has been quite complicated at first, until I picked up speed.

### What would you have liked to do if you had the time?
A LOT. First I would have created a pager. I think it's the perfect component to show an employee your skills using web components. The design leaves a lot to be desired, but I'm not very good at it, so I would have liked to search the internet for some inspiration. As usual I haven't had much more time. 
I would have liked to create my own toast component. I would have loved to create a system like a real API.
The starting data is pretty bad

The list never ends TBH


