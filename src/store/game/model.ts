import Game from '@/models/Game'

export default interface GameState {
    list: Game[]
}
