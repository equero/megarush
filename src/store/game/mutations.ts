import Game from "@/models/Game";
import GameState from "./model";
import { gameList } from "@/assets/games";

const initialData: GameState = {
    "list": gameList as Game[]
  }

export const mutations = {
    EDIT_GAME (state: GameState, data: Game): void {
        const index = state.list.findIndex(element => element.id === data.id);
        if (index > -1) {
            state.list[index] = data;
            localStorage.setItem('gameList', JSON.stringify(state.list));
        }
    },
    LOAD_GAMES (state: GameState, data: Game[]): void {
        state.list = data;
    },
    INIT_GAMES (state: GameState): void {
        state.list = initialData.list;
        localStorage.setItem('gameList', JSON.stringify(initialData.list));
    },
    DELETE_GAME (state: GameState, id: string): void {
        const index = state.list.findIndex(element => element.id === id);
        if (index > -1) {
            state.list.splice(index, 1);
            localStorage.setItem('gameList', JSON.stringify(state.list));
        }
    },
    SAVE_NEW_GAME (state: GameState, data: Game): void {
        state.list.push(data);
        localStorage.setItem('gameList', JSON.stringify(state.list));
    }
};
