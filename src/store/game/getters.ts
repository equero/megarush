import Game from '@/models/Game'
import GameState from './model'

export const getters = {
    gameList: (state: GameState): Game[] => state.list,
    gameById: (state: GameState) => (id: string) => state.list.filter(game => game.id === id)[0]
};
