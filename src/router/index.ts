import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Home from '../views/Home.vue';
import GameDetails from '../views/games/GameDetails.vue';
import GameEdit from '../views/games/GameEdit.vue';
import GameNew from '../views/games/GameNew.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/game/:id',
    name: 'Game-details',
    component: GameDetails
  },
  {
    path: '/game/:id/edit',
    name: 'Game-edit',
    component: GameEdit
  },
  {
    path: '/new',
    name: 'Game-new',
    component: GameNew
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
