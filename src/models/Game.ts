interface Author {
    name: string,
    email: string
}

export default interface Game {
    id: string,
    title: string,
    description: string,
    preview_img_url: string,
    url: string,
    author: Author
}
