import Game from '@/models/Game';

export const gameList: Game[] = [
  {
    "id": "2wfc4dk0z",
    "title": "Binding of Isaac",
    "description": "When Isaac’s mother starts hearing the voice of God demanding a sacrifice be made to prove her faith, Isaac escapes into the basement facing droves of deranged enemies, lost brothers and sisters, his fears, and eventually his mother.The Binding of Isaac is a randomly generated action RPG shooter with heavy Rogue-like elements. Following Isaac on his journey players will find bizarre treasures that change Isaac’s form giving him super human abilities and enabling him to fight off droves of mysterious creatures, discover secrets and fight his way to safety.",
    "preview_img_url": "https://cdn.akamai.steamstatic.com/steam/apps/113200/ss_57c7fb142d6b8f7d38ab62d9f39a055a5b2d4c4c.600x338.jpg?t=1573195411",
    "url": "https://store.steampowered.com/app/113200/The_Binding_of_Isaac/",
    "author": {
      "email": "edmund@edmundm.com",
      "name": "Edmund McMillen"
    }
  },
  {
    "id": "gn6a3z3ic",
    "title": "LIMBO",
    "description": "Uncertain of his sister's fate, a boy enters LIMBO",
    "preview_img_url": "https://cdn.akamai.steamstatic.com/steam/apps/48000/header.jpg?t=1617191668",
    "url": "https://store.steampowered.com/app/48000/LIMBO/",
    "author": {
      "email": "press@playdead.com",
      "name": "Playdead"
    }
  },
  {
    "id": "10t7xsdd4",
    "title": "Spiritfarer",
    "description": "Spiritfarer is a cozy management game about dying. You play Stella, ferrymaster to the deceased, a Spiritfarer. Build a boat to explore the world, then befriend and care for spirits before finally releasing them into the afterlife. Farm, mine, fish, harvest, cook, and craft your way across mystical seas. Join the adventure as Daffodil the cat, in two-player cooperative play. Spend relaxing quality time with your spirit passengers, create lasting memories, and, ultimately, learn how to say goodbye to your cherished friends. What will you leave behind?",
    "preview_img_url": "https://cdn.akamai.steamstatic.com/steam/apps/972660/header_alt_assets_2.jpg?t=1619214409",
    "url": "https://store.steampowered.com/app/972660/Spiritfarer/",
    "author": {
      "email": "press@playdead.com",
      "name": "contact@thunderlotusgames.com"
    }
  },
  {
    "id": "0j1ekoxrb",
    "title": "Hollow Knight",
    "description": "Beneath the fading town of Dirtmouth sleeps an ancient, ruined kingdom. Many are drawn below the surface, searching for riches, or glory, or answers to old secrets. Hollow Knight is a classically styled 2D action adventure across a vast interconnected world. Explore twisting caverns, ancient cities and deadly wastes; battle tainted creatures and befriend bizarre bugs; and solve ancient mysteries at the kingdom's heart.",
    "preview_img_url": "https://cdn.akamai.steamstatic.com/steam/apps/367520/header.jpg?t=1601950406",
    "url": "https://store.steampowered.com/app/367520/Hollow_Knight/",
    "author": {
      "email": "press@teamcherry.com",
      "name": "Team Cherry"
    }
  },
  {
    "id": "9m38qni9w",
    "title": "Braid",
    "description": "Braid is a puzzle-platformer, drawn in a painterly style, where you can manipulate the flow of time in strange and unusual ways. From a house in the city, journey to a series of worlds and solve puzzles to rescue an abducted princess. In each world, you have a different power to affect the way time behaves, and it is time's strangeness that creates the puzzles. The time behaviors include: the ability to rewind, objects that are immune to being rewound, time that is tied to space, parallel realities, time dilation, and perhaps more. Braid treats your time and attention as precious; there is no filler in this game. Every puzzle shows you something new and interesting about the game world.",
    "preview_img_url": "https://cdn.akamai.steamstatic.com/steam/apps/26800/header.jpg?t=1515716884",
    "url": "https://store.steampowered.com/app/26800/Braid/",
    "author": {
      "email": "numbernone@test.com",
      "name": "Number None"
    }
  }
]